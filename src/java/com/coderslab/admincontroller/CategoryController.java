package com.coderslab.admincontroller;

import com.coderslab.dao.CategoryDao;
import com.coderslab.entity.Category;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping("/admin")
public class CategoryController {

    @Autowired
    private CategoryDao categoryDao;
    
    //category
    @RequestMapping(value = "/add_category", method = RequestMethod.GET)
    public String loadAddCategoryPage(Model model){
        model.addAttribute("category", new Category());
        return "admin/add_category";
    }
    
    @RequestMapping(value = "/list_category", method = RequestMethod.GET)
    public String loadListCategoryPage(Model model, HttpServletRequest request){
        model.addAttribute("categories", categoryDao.getAllCategory());
        model.addAttribute("sm", request.getParameter("sm"));
        model.addAttribute("em", request.getParameter("em"));
        return "admin/list_category";
    }
    
    
    @RequestMapping(value = "/save_category", method = RequestMethod.POST)
    public String saveCategory(Model model, Category category){
        
        if(category.getCategory_name().isEmpty()){
            model.addAttribute("em", "Empty Category Name");
            return "admin/add_category";
        }
        
        category.setPublication_status(true);
        boolean status = categoryDao.saveCategory(category);
        if(status){
            model.addAttribute("sm", "Category created Successfully");
        }else{
            model.addAttribute("em", "Category Not create");
        }
        model.addAttribute("category", new Category());
        return "admin/add_category";
    }
    
    
    @RequestMapping(value = "/update_category_status/{id}/{status}", method = RequestMethod.GET)
    public String updateCategoryStatus(Model model, @PathVariable("id") int id, @PathVariable("status") boolean status){
        
        boolean publication_status = !status;
        
        Category c = new Category();
        c.setCategory_id(id);
        c.setPublication_status(publication_status);
        
        boolean stat = categoryDao.updateCategoryStatus(c);
        if(stat){
            model.addAttribute("sm", "Category Status Update Successfully");
        }else{
            model.addAttribute("em", "Category Status not change");
        }
        
        
        
        System.out.println("=======" + String.valueOf(publication_status));
        return "redirect:/admin/list_category";
    }
    
    @RequestMapping(value = "/edit_category/{id}", method = RequestMethod.GET)
    public String editCategoryPageLoad(HttpServletRequest request, Model model, @PathVariable("id") int id, Category category){
        
        category = categoryDao.getCategoryById(id);
        
        System.out.println("========" + category.toString());
        
        model.addAttribute("category", category);
        model.addAttribute("sm", request.getParameter("sm"));
        model.addAttribute("em", request.getParameter("em"));
        
        return "admin/edit_category";
    }
    
    
    @RequestMapping(value = "/update_category", method = RequestMethod.POST)
    public String updateCategory(Category category, Model model){
        
        boolean status = categoryDao.updateCategory(category);
        
        if(status){
            model.addAttribute("sm", "Category Update Successfull");
        }else{
            model.addAttribute("em", "Category Not Update");
        }
        
        return "redirect:/admin/edit_category/" + category.getCategory_id();
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
