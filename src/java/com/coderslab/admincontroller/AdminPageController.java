
package com.coderslab.admincontroller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/admin")
public class AdminPageController {
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String loadHomePage(){
        return "admin/index";
    }
    
    
    
    
    
    
    
    
    //Order
    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public String loadOrderPage(){
        return "admin/orders";
    }
    
    @RequestMapping(value = "/order_details", method = RequestMethod.GET)
    public String loadOrderDetailsPage(){
        return "admin/order_details";
    }
    
    
    
    
    
}
