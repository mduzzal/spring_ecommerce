package com.coderslab.admincontroller;

import com.coderslab.dao.CategoryDao;
import com.coderslab.dao.SubCategoryDao;
import com.coderslab.entity.Category;
import com.coderslab.entity.SubCategory;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/admin")
public class SubCategoryController {

    @Autowired
    private CategoryDao categoryDao;
    @Autowired
    private SubCategoryDao SubCategoryDao;

    //sub category
    @RequestMapping(value = "/add_sub_category", method = RequestMethod.GET)
    public String loadAddSubCategoryPage(Model model, HttpServletRequest request) {

        model.addAttribute("categories", categoryDao.getAllCategory());
        model.addAttribute("sm", request.getParameter("sm"));
        model.addAttribute("em", request.getParameter("em"));
        return "admin/add_sub_category";
    }

    @RequestMapping(value = "/list_sub_category", method = RequestMethod.GET)
    public String loadListSubCategoryPage(Model model, HttpServletRequest request) {
        model.addAttribute("subCategories", SubCategoryDao.getAllSubCategory());
        model.addAttribute("sm", request.getParameter("sm"));
        model.addAttribute("em", request.getParameter("em"));
        return "admin/list_sub_category";
    }

    @RequestMapping(value = "/save_sub_category", method = RequestMethod.POST)
    public String saveSubCategory(Model model, SubCategory subCategory) {

        if (subCategory.getCategory_id() == 0) {
            model.addAttribute("em", "Please Select Category Name");
            return "redirect:/admin/add_sub_category";
        }
        if (subCategory.getSub_category_name().isEmpty()) {
            model.addAttribute("em", "Please Insert Sub Category Name");
            return "redirect:/admin/add_sub_category";
        }

        subCategory.setPublication_status(true);

        System.out.println("==========" + subCategory.toString());

        boolean status = SubCategoryDao.saveSubCategory(subCategory);
        if (status) {
            model.addAttribute("sm", "Sub Category Saved Successfully");
        } else {
            model.addAttribute("em", "Sub Category Not Saved");
        }
        return "redirect:/admin/add_sub_category";
    }

    @RequestMapping(value = "/update_sub_category_status/{id}/{status}", method = RequestMethod.GET)
    public String updateSubCategoryStatus(Model model, @PathVariable("id") int id, @PathVariable("status") boolean status) {

        boolean publication_status = !status;

        SubCategory sc = new SubCategory();
        sc.setSub_category_id(id);
        sc.setPublication_status(publication_status);

        boolean stat = SubCategoryDao.updateSubCategoryStatus(sc);
        if (stat) {
            model.addAttribute("sm", "Sub Category Status Update Successfully");
        } else {
            model.addAttribute("em", "Sub Category Status not change");
        }

        System.out.println("=======" + String.valueOf(publication_status));
        return "redirect:/admin/list_sub_category";
    }

    @RequestMapping(value = "/edit_sub_category/{id}", method = RequestMethod.GET)
    public String editSubCategoryPageLoad(HttpServletRequest request, Model model, @PathVariable("id") int id, SubCategory subCategory) {

        subCategory = SubCategoryDao.getSubCategoryById(id);
        model.addAttribute("categories", categoryDao.getAllCategory());
        model.addAttribute("subCategory", subCategory);
        model.addAttribute("sm", request.getParameter("sm"));
        model.addAttribute("em", request.getParameter("em"));

        return "admin/edit_sub_category";
    }
    
    
    
    @RequestMapping(value = "/update_sub_category", method = RequestMethod.POST)
    public String updateSubCategory(SubCategory subCategory, Model model){
        
        boolean status = SubCategoryDao.updateSubCategory(subCategory);
        
        if(status){
            model.addAttribute("sm", "Sub Category Update Successfull");
        }else{
            model.addAttribute("em", "Sub Category Not Update");
        }
        
        return "redirect:/admin/edit_sub_category/" + subCategory.getSub_category_id();
        
    }
    
    
    
    
    

}
