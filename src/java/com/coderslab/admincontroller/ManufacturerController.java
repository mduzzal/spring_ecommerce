package com.coderslab.admincontroller;

import com.coderslab.dao.ManufacturerDao;
import com.coderslab.entity.Manufacturer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/admin")
public class ManufacturerController {

    @Autowired
    private ManufacturerDao manufacturerDao;

    //Manufacture
    @RequestMapping(value = "/add_mf", method = RequestMethod.GET)
    public String loadAddMFPage(Model model, Manufacturer manufacturer, HttpServletRequest request) {
        model.addAttribute("manufacturer", new Manufacturer());
        model.addAttribute("sm", request.getParameter("sm"));
        model.addAttribute("em", request.getParameter("em"));
        return "admin/add_mf";
    }

    @RequestMapping(value = "/list_mf", method = RequestMethod.GET)
    public String loadListMFPage() {
        return "admin/list_mf";
    }

    @RequestMapping(value = "/save_mf", method = RequestMethod.POST)
    public String saveMf(Model model, Manufacturer manufacturer) {

        if (!manufacturer.getMf_name().isEmpty()) {
            try {
                manufacturer.setPublication_status(true);
                manufacturerDao.saveData(manufacturer);
                model.addAttribute("sm", "Manufacturer save successfully");
            } catch (Exception ex) {
                ex.printStackTrace();
                model.addAttribute("em", "Manufacturer not saved");
            }
        } else {
            model.addAttribute("em", "Please enter manufacturer name");
        }

        return "redirect:/admin/add_mf";
    }

}
