
package com.coderslab.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class FrontPageController {
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String loadHomePage(){
        return "index";
    }
    
    
    
    
    
}
