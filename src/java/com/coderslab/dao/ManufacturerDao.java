/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coderslab.dao;

import com.coderslab.entity.Manufacturer;
import java.io.Serializable;
import org.springframework.stereotype.Repository;

@Repository
public class ManufacturerDao extends GenericDao<Manufacturer, Serializable>{

    @Override
    public boolean saveData(Manufacturer obj) throws Exception {
        return super.saveData(obj); //To change body of generated methods, choose Tools | Templates.
    }
    
}
