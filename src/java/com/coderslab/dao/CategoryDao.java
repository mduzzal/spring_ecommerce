package com.coderslab.dao;

import com.coderslab.entity.Category;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class CategoryDao {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    public List<Category> getAllCategory(){
        return sessionFactory.getCurrentSession().createQuery("from Category").list();
    }

    public boolean saveCategory(Category category) {
        System.out.println("========" + category.toString());
        sessionFactory.getCurrentSession().save(category);
        return true;

    }
    
    public boolean updateCategoryStatus(Category category){
        Query query = sessionFactory.getCurrentSession().createSQLQuery("update category set publication_status = :publication_status where category_id = :category_id");
        query.setBoolean("publication_status", category.isPublication_status());
        query.setInteger("category_id", category.getCategory_id());
        query.executeUpdate();
        return true;
    }
    
    public Category getCategoryById(int id){
        return (Category) sessionFactory.getCurrentSession().get(Category.class, id);
    }
    
    public boolean updateCategory(Category category){
        Query query = sessionFactory.getCurrentSession().createSQLQuery("update category set category_name = :category_name, category_ds = :category_ds where category_id=:category_id");
        query.setString("category_name", category.getCategory_name());
        query.setString("category_ds", category.getCategory_ds());
        query.setInteger("category_id", category.getCategory_id());
        query.executeUpdate();
        return true;
    }

}
