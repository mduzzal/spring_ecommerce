package com.coderslab.dao;

import com.coderslab.entity.Category;
import com.coderslab.entity.SubCategory;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class SubCategoryDao {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    public List<SubCategory> getAllSubCategory(){
        return sessionFactory.getCurrentSession().createQuery("from SubCategory").list();
    }

    public boolean saveSubCategory(SubCategory subCategory) {
        System.out.println("========" + subCategory.toString());
        sessionFactory.getCurrentSession().save(subCategory);
        return true;

    }
    
    public boolean updateSubCategoryStatus(SubCategory subCategory){
        Query query = sessionFactory.getCurrentSession().createSQLQuery("update sub_category set publication_status = :publication_status where sub_category_id = :sub_category_id");
        query.setBoolean("publication_status", subCategory.isPublication_status());
        query.setInteger("sub_category_id", subCategory.getSub_category_id());
        query.executeUpdate();
        return true;
    }
    
    public SubCategory getSubCategoryById(int id){
        return (SubCategory) sessionFactory.getCurrentSession().get(SubCategory.class, id);
    }
    
    public boolean updateSubCategory(SubCategory subCategory){
        Query query = sessionFactory.getCurrentSession().createSQLQuery("update sub_category set category_id=:category_id, sub_category_name = :sub_category_name, sub_category_ds = :sub_category_ds where sub_category_id=:sub_category_id");
        query.setInteger("category_id", subCategory.getCategory_id());
        query.setString("sub_category_name", subCategory.getSub_category_name());
        query.setString("sub_category_ds", subCategory.getSub_category_ds());
        query.setInteger("sub_category_id", subCategory.getSub_category_id());
        query.executeUpdate();
        return true;
    }
    
    

}
