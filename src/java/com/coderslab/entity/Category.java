package com.coderslab.entity;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;

@Entity
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int category_id;
    @Size(min = 1, max = 40, message = "You must fill up category name")
    private String category_name;
    private String category_ds;
    private boolean publication_status;

    public Category() {
    }

    public Category(int category_id, String category_name, String category_ds, boolean publication_status) {
        this.category_id = category_id;
        this.category_name = category_name;
        this.category_ds = category_ds;
        this.publication_status = publication_status;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCategory_ds() {
        return category_ds;
    }

    public void setCategory_ds(String category_ds) {
        this.category_ds = category_ds;
    }

    public boolean isPublication_status() {
        return publication_status;
    }

    public void setPublication_status(boolean publication_status) {
        this.publication_status = publication_status;
    }

    @Override
    public String toString() {
        return "Category{" + "category_id=" + category_id + ", category_name=" + category_name + ", category_ds=" + category_ds + ", publication_status=" + publication_status + '}';
    }

}
