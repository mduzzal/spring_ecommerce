package com.coderslab.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Manufacturer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int mf_id;
    private String mf_name;
    private String mf_ds;
    private boolean publication_status;

    public Manufacturer() {
    }

    public Manufacturer(int mf_id, String mf_name, String mf_ds, boolean publication_status) {
        this.mf_id = mf_id;
        this.mf_name = mf_name;
        this.mf_ds = mf_ds;
        this.publication_status = publication_status;
    }

    public int getMf_id() {
        return mf_id;
    }

    public void setMf_id(int mf_id) {
        this.mf_id = mf_id;
    }

    public String getMf_name() {
        return mf_name;
    }

    public void setMf_name(String mf_name) {
        this.mf_name = mf_name;
    }

    public String getMf_ds() {
        return mf_ds;
    }

    public void setMf_ds(String mf_ds) {
        this.mf_ds = mf_ds;
    }

    public boolean isPublication_status() {
        return publication_status;
    }

    public void setPublication_status(boolean publication_status) {
        this.publication_status = publication_status;
    }

    @Override
    public String toString() {
        return "Manufacturer{" + "mf_id=" + mf_id + ", mf_name=" + mf_name + ", mf_ds=" + mf_ds + ", publication_status=" + publication_status + '}';
    }

}
