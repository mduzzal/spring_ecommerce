package com.coderslab.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sub_category")
public class SubCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int sub_category_id;
    private int category_id;
    private String sub_category_name;
    private String sub_category_ds;
    private boolean publication_status;

    public SubCategory() {
    }

    public SubCategory(int sub_category_id, int category_id, String sub_category_name, String sub_category_ds, boolean publication_status) {
        this.sub_category_id = sub_category_id;
        this.category_id = category_id;
        this.sub_category_name = sub_category_name;
        this.sub_category_ds = sub_category_ds;
        this.publication_status = publication_status;
    }

    public int getSub_category_id() {
        return sub_category_id;
    }

    public void setSub_category_id(int sub_category_id) {
        this.sub_category_id = sub_category_id;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getSub_category_name() {
        return sub_category_name;
    }

    public void setSub_category_name(String sub_category_name) {
        this.sub_category_name = sub_category_name;
    }

    public String getSub_category_ds() {
        return sub_category_ds;
    }

    public void setSub_category_ds(String sub_category_ds) {
        this.sub_category_ds = sub_category_ds;
    }

    public boolean isPublication_status() {
        return publication_status;
    }

    public void setPublication_status(boolean publication_status) {
        this.publication_status = publication_status;
    }

    @Override
    public String toString() {
        return "SubCategory{" + "sub_category_id=" + sub_category_id + ", category_id=" + category_id + ", sub_category_name=" + sub_category_name + ", sub_category_ds=" + sub_category_ds + ", publication_status=" + publication_status + '}';
    }

}
