<%-- 
    Document   : index
    Created on : Sep 24, 2017, 10:52:03 PM
    Author     : cyclingbd007
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<!DOCTYPE html>
<html class="no-js" lang="">

    <!-- Mirrored from devitems.com/html/b-sale-preview/b-sale/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 18 Aug 2016 02:23:09 GMT -->
    <head>

        <!-- Fonts
        ============================================ -->
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,700,600,500,300,800,900' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,500,300,300italic,500italic,700' rel='stylesheet' type='text/css'>

        <!-- CSS  -->

        <!-- Bootstrap CSS
        ============================================ -->    
        <spring:url value="/resources/front_resources/css/bootstrap.min.css" var="bootstrapMinCss"/>
        <link rel="stylesheet" type="text/css" href="${bootstrapMinCss}"/>

        <!-- font-awesome.min CSS
        ============================================ -->    
        <spring:url value="/resources/front_resources/css/font-awesome.min.css" var="fontAwesomeCSS"/>
        <link rel="stylesheet" href="${fontAwesomeCSS}">

        <!-- Mean Menu CSS
        ============================================ -->   
        <spring:url value="/resources/front_resources/css/meanmenu.min.css" var="meanmenuCSS"/>
        <link rel="stylesheet" href="${meanmenuCSS}">

        <!-- owl.carousel CSS
        ============================================ -->    
        <spring:url value="/resources/front_resources/css/owl.carousel.css" var="carouselCSS"/>
        <link rel="stylesheet" href="${carouselCSS}">

        <!-- owl.theme CSS
        ============================================ -->   
        <spring:url value="/resources/front_resources/css/owl.theme.css" var="owlThemeCSS"/>
        <link rel="stylesheet" href="${owlThemeCSS}">

        <!-- owl.transitions CSS
        ============================================ -->   
        <spring:url value="/resources/front_resources/css/owl.transitions.css" var="transitionsCSS"/>
        <link rel="stylesheet" href="${transitionsCSS}">

        <!-- Price Filter CSS
        ============================================ --> 
        <spring:url value="/resources/front_resources/css/jquery-ui.min.css" var="jQueryUiCSS"/>
        <link rel="stylesheet" href="${jQueryUiCSS}">	

        <!-- nivo-slider css
        ============================================ --> 
        <spring:url value="/resources/front_resources/css/nivo-slider.css" var="nivoSliderCSS"/>
        <link rel="stylesheet" href="${nivoSliderCSS}">

        <!-- animate CSS
        ============================================ -->  
        <spring:url value="/resources/front_resources/css/animate.css" var="animateCSS"/>
        <link rel="stylesheet" href="${animateCSS}">

        <!-- jquery-ui-slider CSS
        ============================================ --> 
        <spring:url value="/resources/front_resources/css/jquery-ui-slider.css" var="jqueryUISliderCSS"/>
        <link rel="stylesheet" href="${jqueryUISliderCSS}">

        <!-- normalize CSS
        ============================================ -->   
        <spring:url value="/resources/front_resources/css/normalize.css" var="normalizeCSS"/>
        <link rel="stylesheet" href="${normalizeCSS}">

        <!-- main CSS
                ============================================ -->  
        <spring:url value="/resources/front_resources/css/main.css" var="mainCSS"/>        
        <link rel="stylesheet" href="${mainCSS}">

        <!-- style CSS
                ============================================ -->    
        <spring:url value="/resources/front_resources/style.css" var="styleCSS"/>
        <link rel="stylesheet" href="${styleCSS}">

        <!-- responsive CSS
                ============================================ -->
        <spring:url value="/resources/front_resources/css/responsive.css" var="responsiveCSS"/>
        <link rel="stylesheet" href="${responsiveCSS}">

        <spring:url value="/resources/front_resources/js/vendor/modernizr-2.8.3.min.js" var="modernizerJS"/>
        <script src="${modernizerJS}"></script>
    </head>
    <body class="home-one">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- HEADER AREA -->
        <div class="header-area">
            <div class="header-top-bar">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="header-top-left">
                                <div class="header-top-menu">
                                    <ul class="list-inline">
                                        <li><img src="<c:url value="/resources/front_resources/img/flag.png"/>" alt="flag"></li>
                                        <li class="dropdown"><a href="#" data-toggle="dropdown">English</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Spanish</a></li>
                                                <li><a href="#">China</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown"><a href="#" data-toggle="dropdown">USD</a>
                                            <ul class="dropdown-menu usd-dropdown">
                                                <li><a href="#">USD</a></li>
                                                <li><a href="#">GBP</a></li>
                                                <li><a href="#">EUR</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <p>Welcome visitor!</p>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <div class="header-top-right">
                                <ul class="list-inline">
                                    <li><a href="#"><i class="fa fa-user"></i>My Account</a></li>
                                    <li><a href="#"><i class="fa fa-heart"></i>Wishlist</a></li>
                                    <li><a href="checkout.html"><i class="fa fa-check-square-o"></i>Checkout</a></li>
                                    <li><a href="#"><i class="fa fa-lock"></i>Login</a></li>
                                    <li><a href="#"><i class="fa fa-pencil-square-o"></i>Register</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="header-logo">
                                <a href="index.html"><img src="<c:url value="/resources/front_resources/img/logo.png"/>" alt="logo"/></a>
                                    
                                </div>
                        </div>
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            <div class="search-chart-list">
                                <div class="catagori-menu">
                                    <ul class="list-inline">
                                        <li><i class="fa fa-search"></i></li>
                                        <li>
                                            <select>
                                                <option value="All Categories">All Categories</option>
                                                <option value="Categorie One">Categorie One</option>
                                                <option value="Categorie Two">Categorie Two</option>
                                                <option value="Categorie Three">Categorie Three</option>
                                                <option value="Categorie Four">Categorie Four</option>
                                                <option value="Categorie Five">Categorie Five</option>
                                            </select>
                                        </li>
                                    </ul>
                                </div>
                                <div class="header-search">
                                    <form action="#">
                                        <input type="text" placeholder="My Search"/>
                                        <button type="button"><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                                <div class="header-chart">
                                    <ul class="list-inline">
                                        <li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
                                        <li class="chart-li"><a href="#">My cart</a>
                                            <ul>
                                                <li>
                                                    <div class="header-chart-dropdown">
                                                        <div class="header-chart-dropdown-list">
                                                            <div class="dropdown-chart-left floatleft">
                                                                <a href="#"><img src="<c:url value="/resources/front_resources/img/product/best-product-1.png"/>" alt="list"></a>
                                                            </div>
                                                            <div class="dropdown-chart-right">
                                                                <h2><a href="#">Feugiat justo lacinia</a></h2>
                                                                <h3>Qty: 1</h3>
                                                                <h4>£80.00</h4>
                                                            </div>
                                                        </div>
                                                        <div class="header-chart-dropdown-list">
                                                            <div class="dropdown-chart-left floatleft">
                                                                <a href="#"><img src="<c:url value="/resources/front_resources/img/product/best-product-2.png"/>" alt="list"></a>
                                                            </div>
                                                            <div class="dropdown-chart-right">
                                                                <h2><a href="#">Aenean eu tristique</a></h2>
                                                                <h3>Qty: 1</h3>
                                                                <h4>£70.00</h4>
                                                            </div>
                                                        </div>
                                                        <div class="chart-checkout">
                                                            <p>subtotal<span>£150.00</span></p>
                                                            <button type="button" class="btn btn-default">Chckout</button>
                                                        </div>
                                                    </div> 
                                                </li> 
                                            </ul> 
                                        </li>
                                        <li><a href="#">2 items</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- MAIN MENU AREA -->
        <div class="main-menu-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="main-menu hidden-xs">
                            <nav>
                                <ul>
                                    <li><a href="index.html">Home</a>
                                        <ul class="sub-menu">
                                            <li><a href="index.html">Home Page 1</a></li>
                                            <li><a href="index-2.html">Home Page 2</a></li>
                                            <li><a href="index-3.html">Home Page 3</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="shop.html">Shop</a>
                                        <ul class="mega-menu hidden-xs">
                                            <li>
                                                <div class="mega-menu-list">
                                                    <div class="single-mega-menu">
                                                        <h2>Shop Layouts</h2>
                                                        <a href="#">Full Width</a>
                                                        <a href="#">Sidebar Left</a>
                                                        <a href="#">Sidebar Right</a>
                                                        <a href="#">List View</a>
                                                    </div>
                                                    <div class="single-mega-menu">
                                                        <h2>Shop Pages</h2>
                                                        <a href="#">Category</a>
                                                        <a href="#">My Account</a>
                                                        <a href="#">Wishlist</a>
                                                        <a href="#">Shopping Cart</a>
                                                    </div>
                                                    <div class="single-mega-menu">
                                                        <h2>Product Types</h2>
                                                        <a href="#">Simple Product</a>
                                                        <a href="#">Variable Product</a>
                                                        <a href="#">Grouped Product</a>
                                                        <a href="#">Downloadable</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="shop.html">Men</a></li>
                                    <li><a href="shop.html">Women</a></li>
                                    <li><a href="shop.html">Kids</a></li>
                                    <li><a href="shop.html">gift</a></li>
                                    <li><a href="blog-left-sidebar.html">Blog</a>
                                        <ul class="sub-menu">
                                            <li><a href="blog-right-sidebar.html">Blog Right Sidebar</a></li>
                                            <li><a href="blog-single.html">Blog Details</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Pages</a>
                                        <ul class="sub-menu">
                                            <li><a href="shop.html">Shop</a></li>
                                            <li><a href="shop.html">Men</a></li>
                                            <li><a href="shop.html">Women</a></li>
                                            <li><a href="shop.html">Kids</a></li>
                                            <li><a href="shop.html">Gift</a></li>
                                            <li><a href="about-us.html">About Us</a></li>
                                            <li><a href="single-product.html">Single Product</a></li>
                                            <li><a href="single-shop.html">Single Item</a></li>
                                            <li><a href="cart.html">Cart</a></li>
                                            <li><a href="checkout.html">Checkout</a></li>
                                            <li><a href="look-book.html">Look Book</a></li>
                                            <li><a href="404.html">Error 404</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="contact.html">contact</a></li>
                                </ul>
                            </nav>
                        </div>
                        <!-- Mobile MENU AREA -->
                        <div class="mobile-menu hidden-sm hidden-md hidden-lg">
                            <nav>
                                <ul>
                                    <li><a href="index.html">Home</a>
                                        <ul class="sub-menu">
                                            <li><a href="index.html">Home Page 1</a></li>
                                            <li><a href="index-2.html">Home Page 2</a></li>
                                            <li><a href="index-3.html">Home Page 3</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="shop.html">Shop</a>
                                        <ul>
                                            <li><a href="#">Shop Layouts</a>
                                                <ul>
                                                    <li><a href="#">Full Width</a></li>
                                                    <li><a href="#">Sidebar Left</a></li>
                                                    <li><a href="#">Sidebar Right</a></li>
                                                    <li><a href="#">List View</a></li>
                                                </ul>	
                                            </li>
                                            <li><a href="#">Shop Pages</a>
                                                <ul>
                                                    <li><a href="#">Category</a></li>
                                                    <li><a href="#">My Account</a></li>
                                                    <li><a href="#">Wishlist</a></li>
                                                    <li><a href="#">Shopping Cart</a></li>
                                                </ul>	
                                            </li>
                                            <li><a href="#">Product Types</a>
                                                <ul>
                                                    <li><a href="#">Simple Product</a></li>
                                                    <li><a href="#">Variable Product</a></li>
                                                    <li><a href="#">Grouped Product</a></li>
                                                    <li><a href="#">Downloadable</a></li>
                                                </ul>	
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="shop.html">Men</a></li>
                                    <li><a href="shop.html">Women</a></li>
                                    <li><a href="shop.html">Kids</a></li>
                                    <li><a href="shop.html">gift</a></li>
                                    <li><a href="blog-left-sidebar.html">Blog</a>
                                        <ul>
                                            <li><a href="blog-right-sidebar.html">Blog Right Sidebar</a></li>
                                            <li><a href="blog-single.html">Blog Details</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Pages</a>
                                        <ul>
                                            <li><a href="shop.html">Shop</a></li>
                                            <li><a href="shop.html">Men</a></li>
                                            <li><a href="shop.html">Women</a></li>
                                            <li><a href="shop.html">Kids</a></li>
                                            <li><a href="shop.html">Gift</a></li>
                                            <li><a href="about-us.html">About Us</a></li>
                                            <li><a href="single-product.html">Single Product</a></li>
                                            <li><a href="single-shop.html">Single Item</a></li>
                                            <li><a href="cart.html">Cart</a></li>
                                            <li><a href="checkout.html">Checkout</a></li>
                                            <li><a href="look-book.html">Look Book</a></li>
                                            <li><a href="404.html">Error 404</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="contact.html">contact</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>