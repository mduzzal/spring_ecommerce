<%@include file="header.jsp" %>
<%@include file="main_menu.jsp" %>

<div id="content" class="span10">

    <!-- bread crumb -->
    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#">Add Sub Category</li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Form Elements</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">

                <c:if test="${sm != null}">
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">�</button>
                        <strong>Hurrah! </strong> ${sm}
                    </div>
                </c:if>
                <c:if test="${em != null}">

                    <div class="alert alert-error">
                        <button type="button" class="close" data-dismiss="alert">�</button>
                        <strong>Oh snap!</strong> ${em}
                    </div>
                </c:if>


                <form class="form-horizontal" action="${pageContext.request.contextPath}/admin/save_sub_category" method="post">
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label" for="">Select Category </label>
                            <div class="controls">
                                <select name="category_id">
                                    <option value="0">--- Select Category --</option>
                                    <c:forEach var="row" items="${categories}">
                                    <option value="${row.category_id}">${row.category_name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="category_name">Sub Category Name </label>
                            <div class="controls">
                                <input type="text" name="sub_category_name"/>
                            </div>
                        </div>

                        <div class="control-group hidden-phone">
                            <label class="control-label" for="textarea2">Category Description</label>
                            <div class="controls">
                                <textarea name="sub_category_ds" class="cleditor" id="textarea2" rows="3"></textarea>
                                
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn">Cancel</button>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->





</div>

<%@include file="footer.jsp" %>