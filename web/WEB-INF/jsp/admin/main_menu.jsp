<div id="sidebar-left" class="span2">
    <div class="nav-collapse sidebar-nav">
        <ul class="nav nav-tabs nav-stacked main-menu">
            <li><a href="${pageContext.request.contextPath}/admin/"><i class="icon-bar-chart"></i><span class="hidden-tablet"> Dashboard</span></a></li>	
            
            <li>
                <a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet"> Category </span> <span class="label label-important"> <i class="icon-arrow-down"></i> </span></a>
                <ul>
                    <li><a class="submenu" href="${pageContext.request.contextPath}/admin/add_category"><i class="icon-file-alt"></i><span class="hidden-tablet"> Add Category </span></a></li>
                    <li><a class="submenu" href="${pageContext.request.contextPath}/admin/list_category"><i class="icon-file-alt"></i><span class="hidden-tablet"> Category List </span></a></li>
                </ul>	
            </li>
            
            
            <li>
                <a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet"> Sub Category </span> <span class="label label-important"> <i class="icon-arrow-down"></i> </span></a>
                <ul>
                    <li><a class="submenu" href="${pageContext.request.contextPath}/admin/add_sub_category"><i class="icon-file-alt"></i><span class="hidden-tablet"> Add Sub Category </span></a></li>
                    <li><a class="submenu" href="${pageContext.request.contextPath}/admin/list_sub_category"><i class="icon-file-alt"></i><span class="hidden-tablet"> Sub Category List </span></a></li>
                </ul>	
            </li>
            
            <li>
                <a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet"> Manufacturer </span> <span class="label label-important"> <i class="icon-arrow-down"></i> </span></a>
                <ul>
                    <li><a class="submenu" href="${pageContext.request.contextPath}/admin/add_mf"><i class="icon-file-alt"></i><span class="hidden-tablet"> Add Manufacturer </span></a></li>
                    <li><a class="submenu" href="${pageContext.request.contextPath}/admin/list_mf"><i class="icon-file-alt"></i><span class="hidden-tablet"> Manufacturer List </span></a></li>
                </ul>	
            </li>
            
            <li>
                <a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet"> Order </span> <span class="label label-important"> <i class="icon-arrow-down"></i> </span></a>
                <ul>
                    <li><a class="submenu" href="${pageContext.request.contextPath}/admin/orders"><i class="icon-file-alt"></i><span class="hidden-tablet"> Orders </span></a></li>
                    <li><a class="submenu" href="${pageContext.request.contextPath}/admin/order_details"><i class="icon-file-alt"></i><span class="hidden-tablet"> Order Details </span></a></li>
                </ul>	
            </li>
            
            <li><a href="form.html"><i class="icon-edit"></i><span class="hidden-tablet"> Social Links</span></a></li>
            
            
            
            <li><a href="chart.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> Charts</span></a></li>
            <li><a href="typography.html"><i class="icon-font"></i><span class="hidden-tablet"> Typography</span></a></li>
            <li><a href="gallery.html"><i class="icon-picture"></i><span class="hidden-tablet"> Gallery</span></a></li>
            <li><a href="table.html"><i class="icon-align-justify"></i><span class="hidden-tablet"> Tables</span></a></li>
            <li><a href="calendar.html"><i class="icon-calendar"></i><span class="hidden-tablet"> Calendar</span></a></li>
            <li><a href="file-manager.html"><i class="icon-folder-open"></i><span class="hidden-tablet"> File Manager</span></a></li>
            <li><a href="icon.html"><i class="icon-star"></i><span class="hidden-tablet"> Icons</span></a></li>
            <li><a href="login.html"><i class="icon-lock"></i><span class="hidden-tablet"> Login Page</span></a></li>
        </ul>
    </div>
</div>