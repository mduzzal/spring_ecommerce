<%@include file="header.jsp" %>
<%@include file="main_menu.jsp" %>

<noscript>
<div class="alert alert-block span10">
    <h4 class="alert-heading">Warning!</h4>
    <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
</div>
</noscript>

<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Add Manufacturer</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Form Elements</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">

                <c:if test="${sm != null}">
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">�</button>
                        <strong>Hurrah! </strong> ${sm}
                    </div>
                </c:if>
                <c:if test="${em != null}">

                    <div class="alert alert-error">
                        <button type="button" class="close" data-dismiss="alert">�</button>
                        <strong>Oh snap!</strong> ${em}
                    </div>
                </c:if>


                <form:form class="form-horizontal" action="${pageContext.request.contextPath}/admin/save_mf" method="post" commandName="manufacturer">
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label" for="mf_name">Manufacturer Name </label>
                            <div class="controls">
                                <form:input path="mf_name" type="text" class="form-control" name="mf_name"/>
                                <form:errors path="mf_name"/>
                            </div>
                        </div>

                        <div class="control-group hidden-phone">
                            <label class="control-label" for="mf_ds">Description</label>
                            <div class="controls">
                                <form:textarea path="mf_ds" name="mf_ds" class="cleditor" id="textarea2" rows="3"></form:textarea>
                                <form:errors path="mf_ds"/>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn">Cancel</button>
                        </div>
                    </fieldset>
                </form:form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->



</div>

</div>

<%@include file="footer.jsp" %>