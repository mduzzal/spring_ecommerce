<%@include file="header.jsp" %>
<%@include file="main_menu.jsp" %>

<div id="content" class="span10">

    <!-- bread crumb -->
    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#">Sub Category List</a></li>
    </ul>

    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Members</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <c:if test="${sm != null}">
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">�</button>
                        <strong>Hurrah! </strong> ${sm}
                    </div>
                </c:if>
                <c:if test="${em != null}">

                    <div class="alert alert-error">
                        <button type="button" class="close" data-dismiss="alert">�</button>
                        <strong>Oh snap!</strong> ${em}
                    </div>
                </c:if>
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Sub Category Name</th>
                            <th>Category Name</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <%! int i = 1;%>
                        <c:if test="${subCategories != null}">
                            <c:forEach var="row" items="${subCategories}">


                                <tr>
                                    <td>
                                        <%= i++%>
                                    </td>
                                    <td class="center">${row.sub_category_name}</td>
                                    <td class="center">${row.category_id}</td>
                                    <td class="center">${row.sub_category_ds}</td>
                                    <td class="center">

                                        <c:choose>
                                            <c:when test="${row.publication_status}">
                                                <span class="label label-success">${row.publication_status}</span>
                                            </c:when>
                                            <c:otherwise>
                                                <span class="label label-danger">${row.publication_status}</span>
                                            </c:otherwise>
                                        </c:choose>


                                    </td>
                                    <td class="center">
                                        <c:choose>
                                            <c:when test="${row.publication_status}">
                                                <a class="btn btn-default" href="${pageContext.request.contextPath}/admin/update_sub_category_status/${row.sub_category_id}/${row.publication_status}">
                                                    <i class="halflings-icon white arrow-down"></i>  
                                                </a>
                                            </c:when>
                                            <c:otherwise>
                                                <a class="btn btn-success" href="${pageContext.request.contextPath}/admin/update_sub_category_status/${row.sub_category_id}/${row.publication_status}">
                                                    <i class="halflings-icon white arrow-up"></i>  
                                                </a>
                                            </c:otherwise>
                                        </c:choose>


                                        <a class="btn btn-info" href="${pageContext.request.contextPath}/admin/edit_sub_category/${row.sub_category_id}">
                                            <i class="halflings-icon white edit"></i>  
                                        </a>
                                        <a class="btn btn-danger" href="${pageContext.request.contextPath}/admin/delete_sub_category/${row.category_id}">
                                            <i class="halflings-icon white trash"></i> 
                                        </a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </c:if>
                    </tbody>
                </table>            
            </div>
        </div><!--/span-->
    </div><!--/row-->






</div>

<%@include file="footer.jsp" %>