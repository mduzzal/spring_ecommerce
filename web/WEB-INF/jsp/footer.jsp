<div class="footer-area">
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-12">
                            <div class="footer-info-card">
                                <div class="footer-logo">
                                    <a href="index.html"><img src="<c:url value="/resources/front_resources/img/footer-logo.png"/>" alt="logo"></a>
                                </div>
                                <p>Lorem ipsum dolor sit amet, coetuer adipiscing elit. Aenean comodo liula eget dolor. Aenean massa. Cum sociis natoque penatibus.</p>
                                <ul class="list-inline">
                                    <li><a href="#"><img src="<c:url value="/resources/front_resources/img/visa-card/visa-card-1.png"/>" alt="card" class="img-responsive"></a></li>
                                    <li><a href="#"><img src="<c:url value="/resources/front_resources/img/visa-card/visa-card-2.png"/>" alt="card" class="img-responsive"></a></li>
                                    <li><a href="#"><img src="<c:url value="/resources/front_resources/img/visa-card/visa-card-3.png"/>" alt="card" class="img-responsive"></a></li>
                                    <li><a href="#"><img src="<c:url value="/resources/front_resources/img/visa-card/visa-card-4.png"/>" alt="card" class="img-responsive"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="footer-menu-area">
                                <h2 class="footer-heading">MY ACCOUNT</h2>
                                <div class="footer-menu">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-angle-right"></i>My Account</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right"></i>About Us</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right"></i>Contact</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right"></i>Affiliates</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right"></i>Meet The Maker</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 hidden-sm hidden-xs">
                            <div class="footer-menu-area">
                                <h2 class="footer-heading">Opening time</h2>
                                <div class="footer-menu opening-time">
                                    <ul>
                                        <li><i class="fa fa-angle-right"></i>Satarday<span>7.00 AM - 7.00 PM</span></li>
                                        <li><i class="fa fa-angle-right"></i>Sunday<span>7.00 AM - 7.00 PM</span></li>
                                        <li><i class="fa fa-angle-right"></i>Monday<span>7.00 AM - 7.00 PM</span></li>
                                        <li><i class="fa fa-angle-right"></i>Tuesday<span>7.00 AM - 7.00 PM</span></li>
                                        <li><i class="fa fa-angle-right"></i>Thusday<span>7.00 AM - 7.00 PM</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="contact-info-area">
                                <h2 class="footer-heading">Contact Us</h2>
                                <div class="contact-info">
                                    <div class="contanct-details">
                                        <div class="info-icon">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <div class="info-content">
                                            <p>+1 (00) 86 868 868 666</p>
                                            <p>+1 (00) 42 868 666 888</p>
                                        </div>
                                    </div>
                                    <div class="contanct-details">
                                        <div class="info-icon">
                                            <i class="fa fa-envelope-o"></i>
                                        </div>
                                        <div class="info-content">
                                            <p>admin@bootexperts.com</p>
                                            <p>info@bootexperts.com</p>
                                        </div>
                                    </div>
                                    <div class="contanct-details">
                                        <div class="info-icon">
                                            <i class="fa fa-map-marker"></i>
                                        </div>
                                        <div class="info-content">
                                            <p>68 Dohava Stress, Lorem isput Spust, New York- US</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>	
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="copyright">
                                Copyrignt@2015/<a href="http://bootexperts.com/" target="_blank">BootExperts</a>/ ALL RIGHTS RESERVED
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="footer-social-icon">
                                <ul class="list-inline">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- JS -->

        <!-- jquery-1.11.3.min js
        ============================================ -->   
        <spring:url value="/resources/front_resources/js/vendor/jquery-1.11.3.min.js" var="jqueryJS"/>
        <script src="${jqueryJS}"></script>

        <!-- bootstrap js
        ============================================ -->   
        <spring:url value="/resources/front_resources/js/bootstrap.min.js" var="bootstrapMinJS"/>
        <script src="${bootstrapMinJS}"></script>

        <!-- nivo slider js
        ============================================ --> 
        <spring:url value="/resources/front_resources/js/jquery.nivo.slider.pack.js" var="nivoSliderPack"/>
        <script src="${nivoSliderPack}"></script>

        <!-- Mean Menu js
        ============================================ -->    
        <spring:url value="/resources/front_resources/js/jquery.meanmenu.min.js" var="meanmenuMinJS"/>
        <script src="${meanmenuMinJS}"></script>

        <!-- owl.carousel.min js
        ============================================ -->  
        <spring:url value="/resources/front_resources/js/owl.carousel.min.js" var="carouselJS"/>
        <script src="${carouselJS}"></script>

        <!-- jquery price slider js
        ============================================ --> 
        <spring:url value="/resources/front_resources/js/jquery-price-slider.js" var="jqueryPriceSlider"/>
        <script src="${jqueryPriceSlider}"></script>

        <!-- wow.js
        ============================================ -->
        <spring:url value="/resources/front_resources/js/wow.js" var="wowJS"/>
        <script src="${wowJS}"></script>		
        <script>
            new WOW().init();
        </script>

        <!-- plugins js
        ============================================ -->   
        <spring:url value="/resources/front_resources/js/plugins.js" var="pluginJS"/>
        <script src="${pluginJS}"></script>

        <!-- main js
        ============================================ -->     
        <spring:url value="/resources/front_resources/js/main.js" var="minJS"/>
        <script src="${minJS}"></script>
    </body>

    <!-- Mirrored from devitems.com/html/b-sale-preview/b-sale/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 18 Aug 2016 02:23:40 GMT -->
</html>
